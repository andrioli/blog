require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:posts)
  end

  test "should get new" do
    auth
    get :new
    assert_response :success
  end

  test "should create post" do
    auth
    assert_difference('Post.count') do
      post :create, post: { body: @post.body, title: @post.title }
    end

    assert_redirected_to post_path(assigns(:post))
  end

  test "should show post" do
    get :show, id: @post
    assert_response :success
  end

  test "should get edit" do
    auth
    get :edit, id: @post
    assert_response :success
  end

  test "should update post" do
    auth
    patch :update, id: @post, post: { body: @post.body, title: @post.title }
    assert_redirected_to post_path(assigns(:post))
  end

  test "should destroy post" do
    auth
    assert_difference('Post.count', -1) do
      delete :destroy, id: @post
    end

    assert_redirected_to posts_path
  end

  private
    def auth
      user = 'admin'
      password = 'secret'
      request.headers['Authorization'] = ActionController::HttpAuthentication::Basic.encode_credentials(user, password)
    end
end
